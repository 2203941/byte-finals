var mysql = require("mysql");

var connection = mysql.createConnection({
    host  : "localhost",
    user  : "senchou_admin",
    password  : "captain",
    database  : "senchou_db"
});
  
connection.connect((err)=>{
    if(err){
      console.error("error connecting: " + err.stack);
    }else{
    console.log("Database Connection Successful!");
    console.log("Connected As: " + connection.threadId);
    }
});

module.exports = connection;