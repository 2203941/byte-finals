var express = require("express");
var router = express.Router();
var db = require("./database.js");

//to home page
router.get("/",(req,res) =>{
    console.log("Entered Start Page");
    db.query("SELECT * FROM events",(err,rows)=>{
        if(err){
            console.log(err);
        }else{
                
            if (rows != null){
                db.query("SELECT * FROM event_to_user WHERE user_id = ?",[req.session.userId],(err,results)=>{
                    if (err) {
                       console.error(err);
                    }else{
                        res.render("index",{events : rows , alreadyJoined : results});
                    }
                });          
            }else{
                res.render("index");
            }
        }
    });
});

//to log-in page
router.get("/log-in",(req,res) =>{
    console.log("Entered Log-In Page");
    res.render("log-in");
});

//to about page
router.get("/about",(req,res) =>{
    console.log("Entered About Page");
    res.render("about");
});

//logout function
router.get("/logout",(req,res) =>{
    console.log("User: " + req.session.username + " is logging out");
    req.session.loggedin = false;
    req.session.username = null;
    req.session.userId = null;
    req.session.destroy();
    res.redirect("/");
});

//add event function for admin
router.post("/add-event",(req,res) =>{
    function setAlertMessage(tp, msg){
        req.session.alertMessage = {
            type: tp,
            message: msg
        };
    }
    var name = req.body.eventName;
    var participants = req.body.participants;
    var type = req.body.eventType;
    var query = "INSERT INTO `events` (`event_name`, `participants`, `event_type`) VALUES (?)";
    var values =[ name, participants, type ]
    console.log("Adding New Event");
    if (name && participants && type){
        participants = Number.parseInt(participants);
        if (!isNaN(participants)){
            db.query(query,[values],(err,results)=>{
                if (err) {
                    console.error(err);
                }else{
                    db.query("INSERT INTO `event_to_user` (`event_id`, `user_id`, `category`) VALUES (" + results.insertId + " , " + req.session.userId +", 'creator')",(err)=>{
                        if (err) {
                            console.error(err)
                        }
                    });
                }
            });
            console.log("Successfully added event")
            res.redirect("/");
        }else{
            setAlertMessage("alert-danger","Please input a valid amount of participants");
            res.redirect("/");
        }
        
    }else{
        setAlertMessage("alert-danger","Please input valid values for the fields");
        res.redirect("/");
    }
});

//remove event function for admin
router.get("/remove-event/:eventId",(req,res) =>{
    console.log("Removing Event");
    db.query("DELETE FROM events WHERE event_id = ?",req.params.eventId,(err)=>{
        if (err) {
            console.error(err);
        }else{
            console.log("Event Removal Successful");
            db.query("SELECT * FROM event_to_user WHERE event_id = ? AND user_id = ?", [req.params.eventId,req.session.userId],(err)=>{
                if (err) {
                    console.error(err);
                }else{
                    db.query("DELETE FROM event_to_user WHERE event_id = ? AND user_id = '?'", [req.params.eventId,req.session.userId],(err)=>{
                        if (err) {
                            console.error(err);
                        }
                    });
                }
            });
        }
    });
    res.redirect("/");
});

//cancel participation for event function for members
router.get("/cancel-event/:eventId",(req,res) =>{
    console.log("Cancelling Event Participation");
    db.query("DELETE FROM event_to_user WHERE event_id = ? AND user_id = '?'", [req.params.eventId,req.session.userId],(err)=>{
        if (err) {
            console.log(err);
        }else{
            console.log("Event Participation Removal Successful");
        }
    });
    res.redirect("/");
});

//join event function for members
router.get("/join-event/:eventId",(req,res) =>{
    function setAlertMessage(tp, msg){
        req.session.alertMessage = {
            type: tp,
            message: msg
        };
    }
    var query = "SELECT participants, (SELECT COUNT(*) FROM event_to_user WHERE event_id = '" + req.params.eventId + 
    "' AND category='participant') AS actual_participants FROM `events` WHERE event_id= '" + req.params.eventId +"'";
    db.query(query,(err,results)=>{
        if(!err){
            if ( results[0].actual_participant + 1 > results[0].participants){
                setAlertMessage("alert-danger","You can no longer join! Reserved Slots are now Full!!")
            }else{
                query = "INSERT INTO `event_to_user` (`event_id`, `user_id`, `category`) VALUES (" + req.params.eventId + " , " + req.session.userId +", 'participant')"
                db.query(query,(err)=>{
                if (err) {
                console.log(err);
                }else{
                console.log("Event Participation Successful");
                }
                });
            }
        }else{
            console.error(err);
        }
    });
    
    console.log("Joining Event");
    res.redirect("/");
});

//login function
router.post("/login",(req,res) =>{
    if (req.session.loggedin){
        res.redirect("/");
    }else{
        var username = req.body.username;
	    var password = req.body.password;
        function setAlertMessage(tp, msg){
            req.session.alertMessage = {
                type: tp,
                message: msg
            };
        }

        if (username && password) {
            db.query("SELECT * FROM users WHERE username = ? AND password = ?", [username, password], function(err, results){
                if (results.length > 0) {
                    req.session.loggedin = true;
                    req.session.username = username;
                    req.session.userId = results[0].user_id;
                    if (results[0].admin > 0){
                        req.session.admin = true;
                    }else{
                        req.session.admin = false;
                    }
                    res.redirect('/');
                } else {
                    setAlertMessage("alert-danger","Invalid Username and/or Password");
                    res.redirect("log-in");
                }			
            });
        } else {
            setAlertMessage("alert-danger","Please input a Username and/or Password");
            res.redirect("log-in");
        } 
    }                              
});

module.exports = router;