const express = require("express");
const fs = require("fs");
const path = require("path");
const sass = require("sass");
const session = require("express-session");
const bodyParser = require("body-parser");
var routes = require("./routes.js");

var result = sass.renderSync({file: "./assets/scss/style.scss"});

fs.writeFile("./assets/css/style.css", result.css,function(err){
    if(err){
      console.log(err)
    }
});

var app = express();

//configurations
app.set("port", process.env.PORT || 3000);
app.set("views",path.join(__dirname, "views"));
app.set("view engine", "pug");
app.set('trust proxy', 1)

//middlewares
app.use(session({
  secret: 'kronii is cute',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false }
}));
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());
app.use("/css",express.static(path.resolve(__dirname,"assets/css")));
app.use("/img",express.static(path.resolve(__dirname,"assets/img")));
app.use(( req, res, next )=>{
  res.locals.alertMessage = req.session.alertMessage;
  res.locals.username = req.session.username;
  res.locals.admin = req.session.admin;
  delete req.session.alertMessage;
  next();
});

//routes
app.use(routes);

app.listen(app.get("port"),function(){
    console.log("Server Started on Port: " + app.get("port"));
});